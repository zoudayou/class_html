var currRow = "0";
var currCell = "0";

//清零
function clear() {
	currRow = 0;
	currCell = 0;
}
//设置列宽
function averCol(obj) {
	var wh = obj.width;
	var cells = obj.rows[0].cells;
	for(var i = 0; i < cells.length; i++) {
		cells[i].width = Math.ceil(wh / cells.length);
	}
}
//背景色
function chnBgcolor(obj, index, color) {
	for(var i = 0; i < obj.rows.length; i++) {
		if(i == index) {
			obj.rows[i].bgColor = color;
		} else {
			obj.rows[i].bgColor = "ffffff";
		}
	}
}

//选中效果
function setPos(obj) {
	var o = event.srcElement;
	if(o.tagName == "DIV") {
		currRow = o.parentElement.parentElement.rowIndex;
		currCell = o.parentElement.cellIndex;
		chnBgcolor(obj, currRow, "dedede");
	}
}
function sum(obj){
	setPos(obj);
	if(currRow>0 && currCell>0){
		var rowTotal=0;
		var cellTotal=0;
		var rlen=obj.rows[0].cells.length;
		
		//横向求和
		for(var i=1;i<rlen-1;i++){
			cellTotal+=parseInt(obj.rows[currRow].cells[i].innerText);
		}
		
		obj.rows[currRow].cells[rlen-1].innerText=cellTotal;
		
		//纵向求和
		var clen=obj.rows.length;
		for(var j=1;j<clen-1;j++){
			rowTotal+=parseInt(obj.rows[j].cells[currCell].innerText);
		}
		obj.rows[clen-1].cells[currCell].innerText=rowTotal;
	}

}

//插入行
function insertRow(obj) {
	if(currRow >= 0 && currRow < obj.rows.length - 1) {
		var maxCell = obj.rows[0].cells.length;
		obj.insertRow(currRow + 1);
		for(var i = 0; i < maxCell; i++) {
			obj.rows[currRow + 1].insertCell(i);
			obj.rows[currRow + 1].cells[i].innerHTML = "<div contentEditable>0</div>";
		}
		clear();
		chnBgcolor(obj, currRow, "ffffff");
	}
}

//删除行
function deleteRow(obj) {
	var rows = obj.rows.length;
	if(currRow >= 1 && currRow < rows - 1) {
		obj.deleteRow(currRow);
	}
	clear();
}

//插入列
function insertCell(obj) {
	if(currCell && currCell != obj.rows[0].cells.length - 1) {
		for(var i = 0; i < obj.rows.length; i++) {
			obj.rows[i].insertCell(currCell + 1);
			obj.rows[i].cells[currCell + 1].innerHTML = "<div contentEditable>0</div>";
		}
		clear();
		averCol(obj);
		chnBgcolor(obj, currRow, "ffffff");
	}
}

//删除列
function deleteCell(obj) {
	if(currCell >= 1 && currCell < obj.rows[0].cells.length - 1) {
		for(var i = 0; i < obj.rows.length; i++) {
			obj.rows[i].deleteCell(currCell);
		}
	}
	clear();
}

//正序排行
function ascRow(obj) {
	if(currCell >= 1) {
		var rows = obj.rows;
		var len = rows.length - 2;
		for(var j = 0; j <= rows.length - 2; j++) {
			var order = 0;
			for(var i = 1; i < len; i++) {
				if(parseInt(rows[i].cells[currCell].innerText) > parseInt(rows[i + 1].cells[currCell].innerText)) {
					for(var k = 0; k < obj.rows[i].cells.length; k++) {
						var tmp = obj.rows[i].cells[k].innerHTML;
						obj.rows[i].cells[k].innerHTML = obj.rows[i + 1].cells[k].innerHTML;
						obj.rows[i + 1].cells[k].innerHTML = tmp;
					}
					order++;
				}
			}
			len--;
			if(order == 0) {
				break;
			}
		}
	}
	clear();
}
//倒序排列
function descRow(obj) {
	if(currCell >= 1) {
		var rows = obj.rows;
		var len = rows.length - 2;
		for(var j = 0; j <= rows.length - 2; j++) {
			var order = 0;
			for(var i = 1; i <= len; i++) {
				if(parseInt(rows[i].cells[currCell].innerText) < parseInt(rows[i + 1].cells[currCell].innerText)) {
					for(var k = 0; k < obj.rows[i].cells.length; k++) {
						var tmp = obj.rows[i].cells[k].innerHTML;
						obj.rows[i].cells[k].innerHTML = obj.rows[i + 1].cells[k].innerHTML;
						obj.rows[i + 1].cells[k].innerHTML = tmp;
					}
					order++;
				}
			}
			len--;
			if(order == 0) {
				break;
			}
		}
	}
	clear();
}
//横向正序排列
function ascCell(obj) {
	if(currCell >= 1 && currRow >= 1) {
		var len = obj.rows[0].cells.length;
		for(var j = 0; j < len; j++) {
			var order = 0;
			for(var i = 1; i < len - 2; i++) {
				if(parseInt(obj.rows[currRow].cells[i].innerText) > parseInt(obj.rows[currRow].cells[i + 1].innerText)) {
					for(var k = 0; k < obj.rows.length; k++) {
						var tmp = obj.rows[k].cells[i].innerHTML;
						obj.rows[k].cells[i].innerHTML = obj.rows[k].cells[i + 1].innerHTML;
						obj.rows[k].cells[i + 1].innerHTML = tmp;
					}
					order++;
				}
			}
			len--;
			if(order == 0) {
				break;
			}
		}
	}
	clear();
}

//横向倒序排列
function descCell(obj) {
	if(currCell >= 1 && currRow >= 1) {
		var len = obj.rows[0].cells.length;
		for(var j = 0; j < len; j++) {
			var order = 0;
			for(var i = 1; i < len - 2; i++) {
				if(parseInt(obj.rows[currRow].cells[i].innerText) < parseInt(obj.rows[currRow].cells[i + 1].innerText)) {
					for(var k = 0; k < obj.rows.length; k++) {
						var tmp = obj.rows[k].cells[i].innerHTML;
						obj.rows[k].cells[i].innerHTML = obj.rows[k].cells[i + 1].innerHTML;
						obj.rows[k].cells[i + 1].innerHTML = tmp;
					}
					order++;
				}
			}
			len--;
			if(order == 0) {
				break;
			}
		}
	}
	clear();
}

//求总和
function getSum(obj, objName) {
	var sum = 0;
	for(var i = 1; i < obj.rows.length - 1; i++) {
		for(var j = 1; j < obj.rows[i].cells.length; j++) {
			sum += parseInt(obj.rows[i].cells[j].innerText);
		}
	}
	document.getElementsByName(objName)[0].value = sum;
}